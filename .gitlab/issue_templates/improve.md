#### Problema à resolver

#### Situação atual

#### Situação desejada

#### Origem da necessidade

#### Melhoria/resultado esperado

/estimate

---

### Somente para equipe que ira tratar o problema. Não alterar daqui para baixo.

#### Iniciação

-   [ ] Entendimento da situação
-   [ ] Viabilidade técnica, pré-requisitos
-   [ ] Estimativa e prazos definidos

#### Planejamento

-   [ ] Definir etiquetas
-   [ ] Definir envolvidos
-   [ ] Definir milestone
-   [ ] Definir caso de uso
-   [ ] Definir tarefas
-   [ ] Definir esforço previsto
-   [ ] Definir tarefas relacionadas
-   [ ] Definir data de entrega prevista
-   [ ] Alocar recursos
-   [ ] Criar branch de trabalho
-   [ ] Iniciar execução

#### Execução

-   [ ] TAREFAS
-   [ ] Validação de escopo
-   [ ] Conclusão de tarefa

#### Gestão

-   [ ] Comunicação aos interessados

#### Entrega

-   [ ] Deploy em homologação
-   [ ] Homologação desenvolvimento
-   [ ] Deploy em release
-   [ ] Homologação time de produto
-   [ ] Documentações
