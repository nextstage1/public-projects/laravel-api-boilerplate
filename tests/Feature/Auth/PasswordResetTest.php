<?php

uses(Illuminate\Foundation\Testing\RefreshDatabase::class);

use App\Modules\Company\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


beforeEach(function () {
    $this->company = \App\Modules\Company\Models\Company::factory()->create([
        'name' => 'Testes Unitarios'
    ]);

    $profile = \App\Modules\Company\Models\Profile::factory()->create([
        'name' => 'Testes Unitarios',
        'company_id' => $this->company->id,
        'is_root' => true
    ]);

    $this->user = \App\Modules\Company\Models\User::factory()->create([
        'name' => 'Tester PhpPest',
        'email' => 'tester-pest@local.com',
        'password' => Hash::make('1234567890')
    ]);

    $this->company_user = \App\Modules\Company\Models\UserProfile::factory()->create([
        'user_id' => $this->user->id,
        'profile_id' => $profile->id
    ]);

    // Login to get token
    $modelLogin = [
        'email' => $this->user->email,
        'password' => '1234567890',
        'company_id' => $this->company->id
    ];
    $response = $this->postJson('/api/auth/login', $modelLogin);
    $this->token = $response->json('data.access_token');
});

test('should forgot my password', function () {
    $user = \App\Modules\Company\Models\User::factory()->create([
        'name' => 'Tester PhpPest',
        'email' => 'noexists@notenho.com',
        'password' => 'PENDENTE',
        'email_verified_at' => null
    ]);

    $response = $this->post('/api/auth/forgot-password', [
        'email' => $user->email
    ]);
    $response->assertStatus(200);
    $response->assertJsonFragment(['data' => ['message' => 'Token for password recovery sent. Check email.']]);
});

test('should reset my password', function () {
    $user = \App\Modules\Company\Models\User::factory()->create([
        'name' => 'Tester PhpPest',
        'email' => 'noexists@notenho.com',
        'password' => 'PENDENTE',
        'email_verified_at' => null
    ]);

    $token = '123456';
    DB::table('password_reset_tokens')->where('email', $user->email)->delete();
    $token = fake()->numberBetween(100000, 999999);
    DB::table('password_reset_tokens')->insert([
        'email' => $user->email,
        'token' => (string) $token,
        'created_at' => Carbon::now()
    ]);

    $response = $this->post('/api/auth/reset-password', [
        'email' => $user->email,
        'password' => '10203040',
        'password_confirmation' => '10203040',
        'token' => $token
    ]);

    // var_export($response);
    $response->assertStatus(200);
    $response->assertJson(['data' => ['message' => 'Password successfully updated']]);
});
