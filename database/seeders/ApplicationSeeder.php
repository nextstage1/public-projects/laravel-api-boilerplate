<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class ApplicationSeeder extends Seeder
{
    protected $seeders = [];

    public function run(): void
    {
        // Acceptme testes develop
        if (class_exists(\App\Modules\Acceptme\Models\Product::class)) {
            \App\Modules\Acceptme\Models\RegisterType::factory()->create(['name' => 'Accept']);
            \App\Modules\Acceptme\Models\RegisterType::factory()->create(['name' => 'Reject']);
            $companies = [1, 2];
            foreach ($companies as $companyId) {
                $file = \App\Modules\Application\Models\File::factory()->create(['company_id' => $companyId]);
                $type = \App\Modules\Acceptme\Models\DocumentType::factory()->create([
                    'company_id' => $companyId,
                ]);
                for ($j = 0; $j < 5; $j++) {
                    $product = \App\Modules\Acceptme\Models\Product::factory()->create([
                        'company_id' => $companyId,
                        'file_id' => $file->id
                    ]);
                    for ($k = 0; $k < 5; $k++) {
                        $document = \App\Modules\Acceptme\Models\Document::factory()->create(['product_id' => $product->id, 'document_type_id' => $type->id]);
                        for ($l = 0; $l < 4; $l++) {
                            $version = \App\Modules\Acceptme\Models\Version::factory()->create(['document_id' => $document->id]);
                            // \App\Modules\Acceptme\Models\File::factory()->create(['version_id' => $version->id]);
                            \App\Modules\Acceptme\Models\Register::factory()->count(10)->create(['version_id' => $version->id]);
                        }
                    }
                }
            }
        }
    }
}
