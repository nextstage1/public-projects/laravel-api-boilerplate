<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Expr\AssignOp\Mod;

class DatabaseSeeder extends Seeder
{
    protected $seeders = [];

    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        if (class_exists(\App\Modules\Company\Models\Company::class)) {
            // Seeder defautl to develop
            $ret = \App\Modules\Company\Models\Company::factory()->create(['name' => 'Company: Development']);
            $ret = \App\Modules\Company\Models\Profile::factory()->create(['name' => 'Root', 'company_id' => $ret->id, 'is_root' => true]);
            \App\Modules\Company\Models\User::factory()->create([
                'name' => 'Dev Local',
                'email' => 'dev@local.com',
                'email_verified_at' => now(),
                'password' => Hash::make('10203040')
            ]);
            \App\Modules\Company\Models\UserProfile::factory()->create([
                'user_id' => \App\Modules\Company\Models\User::latest()->first()->id,
                'profile_id' => $ret->id,
            ]);
            // \App\Modules\Application\Models\File::factory(10);

            $ret = \App\Modules\Company\Models\Company::factory()->create(['name' => 'Company: ' . fake()->company()]);
            $ret = \App\Modules\Company\Models\Profile::factory()->create(['name' => 'Profile: ' . fake()->words(5, true), 'company_id' => $ret->id]);
            \App\Modules\Company\Models\UserProfile::factory()->create([
                'user_id' => \App\Modules\Company\Models\User::latest()->first()->id,
                'profile_id' => $ret->id,
            ]);

            // Seeders from Company Module
            $this->call(\App\Modules\Company\Database\Seeders\CompanySeeder::class);

            // Seeders from Application Module
            $this->call(\App\Modules\Application\Database\Seeders\ApplicationSeeder::class);

            // Module Seeder extras
            $this->call(ApplicationSeeder::class);

            // Load other modules
            $this->loadModuleSeeders();
        }
    }

    private function loadModuleSeeders()
    {
        $modulesPath = app_path('Modules');
        foreach (File::directories($modulesPath) as $module) {
            $seederPath = $module . DIRECTORY_SEPARATOR . 'Database' . DIRECTORY_SEPARATOR . 'Seeders';

            if (File::isDirectory($seederPath)) {
                $files = File::files($seederPath);

                foreach ($files as $file) {
                    $filename = pathinfo($file, PATHINFO_FILENAME);
                    if ($filename !== 'CompanySeeder') {
                        $seederClass = '\\App\\Modules\\' . basename($module) . '\\Database\\Seeders\\' . $filename;
                        if (class_exists($seederClass)) {
                            $this->call($seederClass);
                        }
                    }
                }
            }
        }
    }
}
