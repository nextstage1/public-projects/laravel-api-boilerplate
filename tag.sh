#!/bin/sh

get_current_version() {
    git describe --abbrev=0 --tags 2>/dev/null || echo "0.0.0"
}

get_next_version() {
    local version_type="${1:-fix}"
    local current_version="$2"
    local vnum1=0
    local vnum2=0
    local vnum3=0

    IFS='.' read vnum1 vnum2 vnum3 <<-EOF
        $current_version
EOF

    case "$version_type" in
    "version") vnum1=$((vnum1 + 1)) ;;
    "feature") vnum2=$((vnum2 + 1)) ;;
    "fix") vnum3=$((vnum3 + 1)) ;;
    *)
        echo "Invalid version type: $version_type" >&2
        return 1
        ;;
    esac

    echo "$vnum1.$vnum2.$vnum3"
}

get_commit_message() {
    local message="$1"
    local type_commit="${message%%/*}"
    local commit_text="${message#*/}"

    if [ -z "$type_commit" ] || [ -z "$commit_text" ]; then
        read -p "Type of commit (version, feature, or fix): " type_commit
        read -p "Message to commit: " commit_text
    fi

    echo "$type_commit/$commit_text"
}

push_tag() {
    local message="$1"
    local current_version=$(get_current_version)
    local version_type="${message%%/*}"
    local commit_text="${message#*/}"
    local new_version=$(get_next_version "$version_type" "$current_version")
    new_version="$(echo -e "${new_version}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"

    echo "Current version: $current_version"
    echo "New version: $new_version"

    echo "($version_type) updating $current_version to $new_version with message $message"

    git tag "$new_version"
    git push --tags
}

configure_git() {
    echo "URL"
    echo "https://${GITLAB_USER_NAME}:${GITLAB_TOKEN}@${GITLAB_URL}/$(git remote get-url origin | sed 's/.*:\/\/\([^/]*\/\)//;s/\/$//')"
    git config --global user.email "${GITLAB_USER_EMAIL}"
    git config --global user.name "${GITLAB_USER_NAME}"
    git remote set-url origin "https://${GITLAB_USER_NAME}:${GITLAB_TOKEN}@${GITLAB_URL}/$(git remote get-url origin | sed 's/.*:\/\/\([^/]*\/\)//;s/\/$//')"
}

main() {
    local commit_message="$1"
    local tag_pushed=0

    configure_git

    if [ -z "$commit_message" ]; then
        commit_message=$(get_commit_message)
    fi

    while [ $tag_pushed -eq 0 ]; do
        push_tag "$commit_message" && tag_pushed=1
    done

}

main "$@"
