# Laravel Boilerplate with NSUtil Laravel

Os exemplos abaixo foram produzidos baseado em uso de windows com WSL2.

#### Requisitos:

-   Docker
-   Linux ou WSL
-   GIT

#### Passos para instalação:

-   Clonar este repositório `git clone https://gitlab.com/nextstage1/public-projects/laravel-api-boilerplate.git`
-   Alterar o nome do diretorio criado. Ex: `mv laravel-api-boilerplate my-project`
-   Acessar o diretorio `cd my-project`
    -   Remover o diretório .git. Ex.: `sudo rm -R .git`
    -   Inicializar seu git do projeto. Ex.: `git init`
    -   copiar o arquivo .env.example: `cp .env.example .env`
    -   Dar permissão de execução nos arquivos SH: `sudo chmod +x ./docker/scripts/*`
    -   `nano composer.json`: Definir o nome e descrição da aplicação. Ex.: `"name": "minha-empresa/nosso-app-laravel"`
-   Subir aplicação: `sudo ./docker/scripts/start.sh`
-   os logs estão em docker/start.log
-   Acessar o docker da aplicação: `sudo docker exec -it $COMPOSE_PROJECT_NAME_api bash`
-   Execute dentro do docker:
    -   Instalar dependências: `composer install`
    -   Atualizar dependências: `composer update`
    -   Instalar Codebox: `php artisan app:codebox:config`
-   Para aplicação: `sudo ./docker/scripts/stop.sh`
-   Subir aplicação para carga de novos dados: `sudo ./docker/scripts/start.sh`
-   Acessar o docker da aplicação: `sudo docker exec -it $COMPOSE_PROJECT_NAME_api bash`
    -   Executar migrations: `php artisan migrate` _(Caso não exista base de dados ainda)_
    -   Testar: `curl localhost/api/healthcheck`

Pronto, a partir daqui os códigos base estão instalados e prontos para uso.

#### Padrões a serem seguidos:

-   Tabelas sempre no plural
-   Campo ID com o nome "id"
-   Chave estrangeira nas tabelas com o padrão "NOMETABELA_NOSINGULAR_id", ex.: escola_id

#### Próximo passos:

-   Gerar suas migrations. Ex.: `php artisan make:migration create_escolas_table --table=escolas`
-   Configurar o arquivo `.build.config.json` configurando as tabelas em seus módulos
-   Executar migrations: `php artisan migrate` _(Caso não exista base de dados ainda)_
-   Executar o Codebox para gerar os códigos: `php artisan app:build`
-   Executar os testes automatizados: `sudo docker exec app_testing php artisan app:test`

#### Códigos gerados:

-   app/Generated - aqui ficarão os códgiso gerados por padrão. A cada nova construção (app:build) estes arquivos são removidos e reescritos, seguindo o padrão do banco de dados
-   app/Modules - Todas as classes geradas em Generated são abstratas. Aqui neste path ficarão as classes finais que extedem as classes abstratas. Aqui suas alterações não serão sobreescritas.
    -   Uma dica: quanto menos código editado aqui, mais vinculado ao modelo de negócio do banco de dados sua aplicação estará. Caso necessário, reveja modelagem ou crie um fork da NsUtilLaravel para melhorar a geração dos códigos automáticos ;)
-   tests/Feature/Modules - Aqui ficarão as classes de testes geradas automaticamente, testando um CRUD em cada tabela relacionada nos módulos. Todas as classes são reecritas em cada build.
-   coverage - Path contendo uma informação sobre a cobertura do código em testes
-   .postman.collection.json - Um arquivo para importação no Postman com a coleção da documentada para uso e testes
-   storage/api-documentation - Documentação da API em HTML para comunicação com os interessados. Criada com base na configuração dos módulos.
