<?php

    namespace App\Modules\Company\Http\Controllers;

    use App\Generated\Modules\Company\Http\Controllers\AbstractUserController;

    /**
     * Class UserController
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class UserController extends AbstractUserController
    {
    }    
    