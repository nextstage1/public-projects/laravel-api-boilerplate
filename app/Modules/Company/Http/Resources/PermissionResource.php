<?php

    namespace App\Modules\Company\Http\Resources;

    use App\Generated\Modules\Company\Http\Resources\AbstractPermissionResource;
    use Illuminate\Http\Request;
    use Illuminate\Http\Resources\Json\JsonResource;


    /**
     * Class PermissionResource
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class PermissionResource extends AbstractPermissionResource
    {
        /**
         * Transform the resource into an array.
         *
         * @return array<string, mixed>
         */
        public function toArray(Request $request): array
        {
            $data = [];
            if ($this->resource instanceof \Illuminate\Database\Eloquent\Model) {
                $data = array_merge(
                    parent::toArray($request),
                    []
                );
            }
    
            return $data;
        }
    }    
    