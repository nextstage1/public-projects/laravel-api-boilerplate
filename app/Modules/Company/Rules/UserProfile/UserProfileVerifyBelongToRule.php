<?php

    namespace App\Modules\Company\Rules\UserProfile;

    use App\Generated\Modules\Company\Rules\UserProfile\AbstractUserProfileVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class UserProfileVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class UserProfileVerifyBelongToRule extends AbstractUserProfileVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    