<?php

    namespace App\Modules\Company\Rules\ProfilePermission;

    use App\Generated\Modules\Company\Rules\ProfilePermission\AbstractProfilePermissionVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class ProfilePermissionVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class ProfilePermissionVerifyBelongToRule extends AbstractProfilePermissionVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    