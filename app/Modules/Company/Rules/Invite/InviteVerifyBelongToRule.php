<?php

    namespace App\Modules\Company\Rules\Invite;

    use App\Generated\Modules\Company\Rules\Invite\AbstractInviteVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class InviteVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class InviteVerifyBelongToRule extends AbstractInviteVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    