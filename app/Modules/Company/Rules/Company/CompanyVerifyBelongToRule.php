<?php

    namespace App\Modules\Company\Rules\Company;

    use App\Generated\Modules\Company\Rules\Company\AbstractCompanyVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class CompanyVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class CompanyVerifyBelongToRule extends AbstractCompanyVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    