<?php

    namespace App\Modules\Company\Database\Seeders;

    use App\Generated\Modules\Company\Database\Seeders\AbstractCompanySeeder;
        
    final class CompanySeeder extends AbstractCompanySeeder
    {
        public function __construct()
        {
            $this->qtdeToSeed = 100;
        }
    
    }
       
    