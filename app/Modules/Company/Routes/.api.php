<?php

use App\Modules\Company\Http\Controllers\InviteController;
use Illuminate\Support\Facades\Route;

// Route::group([
//     'middleware' => [
//         'Auth.check.token',
//         'Auth.check.permission',
//         'Company.check.belong.to'
//     ]
// ], function () {
//     Route::get('invite/search', [InviteController::class, 'search'])->name('invite.search');
// });

// Route::group([
//     'middleware' => [
//         'Auth.check.token',
//     ]
// ], function () {
//     Route::get('invite-me', [InviteController::class, 'inviteMe'])->name('inviteMe.list');
//     Route::put('invite-me/{id}/decide', [InviteController::class, 'decide'])->name('inviteMe.decide');
// });

$apiGeneratedPath = base_path() . '/app/Generated/Modules/Company/Routes/api.php';
if (file_exists($apiGeneratedPath)) {
    include $apiGeneratedPath;
}
