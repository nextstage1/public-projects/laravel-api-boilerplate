<?php

    namespace App\Modules\Subscription\Database\Seeders;

    use App\Generated\Modules\Subscription\Database\Seeders\AbstractSubscriptionSeeder;
        
    final class SubscriptionSeeder extends AbstractSubscriptionSeeder
    {
        public function __construct()
        {
            $this->qtdeToSeed = 100;
        }
    
    }
       
    