<?php

    namespace App\Modules\Subscription\Providers;

    use App\Generated\Modules\Subscription\Providers\AbstractSubscriptionServiceProvider;

    final class SubscriptionServiceProvider extends AbstractSubscriptionServiceProvider
    {
    }   
    