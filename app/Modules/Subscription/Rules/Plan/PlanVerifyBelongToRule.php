<?php

    namespace App\Modules\Subscription\Rules\Plan;

    use App\Generated\Modules\Subscription\Rules\Plan\AbstractPlanVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class PlanVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class PlanVerifyBelongToRule extends AbstractPlanVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    