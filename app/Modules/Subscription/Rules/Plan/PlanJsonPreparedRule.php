<?php

    namespace App\Modules\Subscription\Rules\Plan;
    
    use Illuminate\Http\Request;
    
    class PlanJsonPreparedRule
    {
        public function __invoke(Request $request): Request
        {            
            if (null !== $request->input('benefits') && is_array($request->input('benefits'))) {
                    $request->merge(['benefits' => json_encode($request->input('benefits'))]);
                }

            return $request;
        }
    }
    