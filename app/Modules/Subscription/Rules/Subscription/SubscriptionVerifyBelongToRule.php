<?php

    namespace App\Modules\Subscription\Rules\Subscription;

    use App\Generated\Modules\Subscription\Rules\Subscription\AbstractSubscriptionVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class SubscriptionVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class SubscriptionVerifyBelongToRule extends AbstractSubscriptionVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    