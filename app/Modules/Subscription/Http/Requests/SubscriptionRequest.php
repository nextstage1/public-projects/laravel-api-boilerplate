<?php

    namespace App\Modules\Subscription\Http\Requests;

    use App\Generated\Modules\Subscription\Http\Requests\AbstractSubscriptionRequest;
    use NsUtilLaravel\Rules\EnumValidation;

    /**
     * Class SubscriptionRequest
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class SubscriptionRequest extends AbstractSubscriptionRequest
    {
        /**
         * Constructor for the SubscriptionRequest class.
         *
         * Initializes the validation rules and error messages specific to this request.
         */
        public function __construct() {

            parent::__construct();

            // Add additional validations rules
            $this->rulesRules = array_merge($this->rulesRules, []);

            // Add any additional custom error messages
            $this->messagesRules = array_merge($this->messagesRules, []);

            // Set the authorization status for the request
            $this->authorizeRule = true;
        }
    }    
    