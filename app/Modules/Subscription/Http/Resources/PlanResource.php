<?php

    namespace App\Modules\Subscription\Http\Resources;

    use App\Generated\Modules\Subscription\Http\Resources\AbstractPlanResource;
    use Illuminate\Http\Request;
    use Illuminate\Http\Resources\Json\JsonResource;


    /**
     * Class PlanResource
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class PlanResource extends AbstractPlanResource
    {
        /**
         * Transform the resource into an array.
         *
         * @return array<string, mixed>
         */
        public function toArray(Request $request): array
        {
            $data = [];
            if ($this->resource instanceof \Illuminate\Database\Eloquent\Model) {
                $data = array_merge(
                    parent::toArray($request),
                    []
                );
            }
    
            return $data;
        }
    }    
    