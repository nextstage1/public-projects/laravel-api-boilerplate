<?php

    namespace App\Modules\Subscription\Http\Controllers;

    use App\Generated\Modules\Subscription\Http\Controllers\AbstractSubscriptionController;

    /**
     * Class SubscriptionController
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class SubscriptionController extends AbstractSubscriptionController
    {
    }    
    