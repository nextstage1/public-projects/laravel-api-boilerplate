<?php

    namespace App\Modules\Application\Rules\Webhook;
    
    use Illuminate\Http\Request;
    
    class WebhookJsonPreparedRule
    {
        public function __invoke(Request $request): Request
        {            
            if (null !== $request->input('data_proccess') && is_array($request->input('data_proccess'))) {
                    $request->merge(['data_proccess' => json_encode($request->input('data_proccess'))]);
                }

            return $request;
        }
    }
    