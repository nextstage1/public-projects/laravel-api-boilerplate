<?php

    namespace App\Modules\Application\Rules\Webhook;

    use App\Generated\Modules\Application\Rules\Webhook\AbstractWebhookVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class WebhookVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class WebhookVerifyBelongToRule extends AbstractWebhookVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    