<?php

    namespace App\Modules\Application\Rules\Queue;

    use App\Generated\Modules\Application\Rules\Queue\AbstractQueueVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class QueueVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class QueueVerifyBelongToRule extends AbstractQueueVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    