<?php

    namespace App\Modules\Application\Rules\Queue;
    
    use Illuminate\Http\Request;
    
    class QueueJsonPreparedRule
    {
        public function __invoke(Request $request): Request
        {            
            if (null !== $request->input('data') && is_array($request->input('data'))) {
                    $request->merge(['data' => json_encode($request->input('data'))]);
                }

            return $request;
        }
    }
    