<?php

    namespace App\Modules\Application\Rules\Address;

    use App\Generated\Modules\Application\Rules\Address\AbstractAddressVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class AddressVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class AddressVerifyBelongToRule extends AbstractAddressVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    