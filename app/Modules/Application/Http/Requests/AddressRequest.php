<?php

    namespace App\Modules\Application\Http\Requests;

    use App\Generated\Modules\Application\Http\Requests\AbstractAddressRequest;
    use NsUtilLaravel\Rules\EnumValidation;

    /**
     * Class AddressRequest
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class AddressRequest extends AbstractAddressRequest
    {
        /**
         * Constructor for the AddressRequest class.
         *
         * Initializes the validation rules and error messages specific to this request.
         */
        public function __construct() {

            parent::__construct();

            // Add additional validations rules
            $this->rulesRules = array_merge($this->rulesRules, []);

            // Add any additional custom error messages
            $this->messagesRules = array_merge($this->messagesRules, []);

            // Set the authorization status for the request
            $this->authorizeRule = true;
        }
    }    
    