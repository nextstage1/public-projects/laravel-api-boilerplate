<?php

    namespace App\Modules\Application\Http\Controllers;

    use App\Generated\Modules\Application\Http\Controllers\AbstractAddressController;

    /**
     * Class AddressController
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class AddressController extends AbstractAddressController
    {
    }    
    