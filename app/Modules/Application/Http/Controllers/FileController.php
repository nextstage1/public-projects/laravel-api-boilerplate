<?php

    namespace App\Modules\Application\Http\Controllers;

    use App\Generated\Modules\Application\Http\Controllers\AbstractFileController;

    /**
     * Class FileController
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class FileController extends AbstractFileController
    {
    }    
    