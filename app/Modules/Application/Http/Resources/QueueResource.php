<?php

    namespace App\Modules\Application\Http\Resources;

    use App\Generated\Modules\Application\Http\Resources\AbstractQueueResource;
    use Illuminate\Http\Request;
    use Illuminate\Http\Resources\Json\JsonResource;


    /**
     * Class QueueResource
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class QueueResource extends AbstractQueueResource
    {
        /**
         * Transform the resource into an array.
         *
         * @return array<string, mixed>
         */
        public function toArray(Request $request): array
        {
            $data = [];
            if ($this->resource instanceof \Illuminate\Database\Eloquent\Model) {
                $data = array_merge(
                    parent::toArray($request),
                    []
                );
            }
    
            return $data;
        }
    }    
    