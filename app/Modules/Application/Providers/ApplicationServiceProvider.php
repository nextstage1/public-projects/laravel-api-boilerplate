<?php

    namespace App\Modules\Application\Providers;

    use App\Generated\Modules\Application\Providers\AbstractApplicationServiceProvider;

    final class ApplicationServiceProvider extends AbstractApplicationServiceProvider
    {
    }   
    