<?php

    namespace App\Modules\Application\Database\Seeders;

    use App\Generated\Modules\Application\Database\Seeders\AbstractApplicationSeeder;
        
    final class ApplicationSeeder extends AbstractApplicationSeeder
    {
        public function __construct()
        {
            $this->qtdeToSeed = 100;
        }
    
    }
       
    