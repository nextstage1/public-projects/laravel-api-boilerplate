<?php

    namespace App\Modules\Acceptme\Http\Requests;

    use App\Generated\Modules\Acceptme\Http\Requests\AbstractDocumentTypeRequest;

    /**
     * Class DocumentTypeRequest
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class DocumentTypeRequest extends AbstractDocumentTypeRequest
    {
    }    
    