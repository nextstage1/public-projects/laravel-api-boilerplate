<?php

    namespace App\Modules\Acceptme\Http\Requests;

    use App\Generated\Modules\Acceptme\Http\Requests\AbstractRegisterTypeRequest;

    /**
     * Class RegisterTypeRequest
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class RegisterTypeRequest extends AbstractRegisterTypeRequest
    {
    }    
    