<?php

    namespace App\Modules\Acceptme\Http\Requests;

    use App\Generated\Modules\Acceptme\Http\Requests\AbstractFileRequest;

    /**
     * Class FileRequest
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class FileRequest extends AbstractFileRequest
    {
    }    
    