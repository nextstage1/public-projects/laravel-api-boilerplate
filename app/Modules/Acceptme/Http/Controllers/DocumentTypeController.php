<?php

    namespace App\Modules\Acceptme\Http\Controllers;

    use App\Generated\Modules\Acceptme\Http\Controllers\AbstractDocumentTypeController;

    /**
     * Class DocumentTypeController
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class DocumentTypeController extends AbstractDocumentTypeController
    {
    }    
    