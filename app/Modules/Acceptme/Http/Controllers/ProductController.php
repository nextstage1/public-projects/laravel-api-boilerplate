<?php

namespace App\Modules\Acceptme\Http\Controllers;

use App\Generated\Modules\Acceptme\Http\Controllers\AbstractProductController;
use App\Modules\Acceptme\Http\Requests\ProductRequest;
use App\Modules\Acceptme\Models\Product;
use Illuminate\Support\Facades\DB;

/**
 * Class ProductController
 *
 * This class extends the generated class. Update this to resolve the requirements of this application module.
 */
final class ProductController extends AbstractProductController
{

    public function __construct()
    {
        parent::__construct();
    }

    protected function getQuery(ProductRequest $request)
    {
        $this->initCondition($request);

        // $this->conditions['document_types.company_id'] =  (int) $request->get('jwt_company_id');

        return Product::select(
            'products.*',
            DB::raw('COUNT (distinct(documents.id)) as documents_count'),
            DB::raw('COUNT (distinct(versions.id)) as versions_count'),
            DB::raw('SUM (versions.views) as views_count'),
            DB::raw('SUM(CASE WHEN registers.register_type_id = 1 THEN 1 ELSE 0 END) as accepts_count'),
            DB::raw('SUM(CASE WHEN registers.register_type_id = 2 THEN 1 ELSE 0 END) as rejects_count')
        )
            ->join('companies', 'companies.id', '=', 'products.company_id')

            ->leftJoin('files', 'files.id', '=', 'products.file_id')
            ->leftJoin('documents', 'documents.product_id', '=', 'products.id')
            ->leftJoin('versions', 'versions.document_id', '=', 'documents.id')
            ->leftJoin('registers', 'registers.version_id', '=', 'versions.id')
            ->leftJoin('document_types', 'document_types.id', '=', 'documents.document_type_id')

            ->groupBy('products.id')
            ->where($this->conditions);
    }
}
