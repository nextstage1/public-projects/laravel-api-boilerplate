<?php

    namespace App\Modules\Acceptme\Http\Controllers;

    use App\Generated\Modules\Acceptme\Http\Controllers\AbstractRegisterTypeController;

    /**
     * Class RegisterTypeController
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class RegisterTypeController extends AbstractRegisterTypeController
    {
    }    
    