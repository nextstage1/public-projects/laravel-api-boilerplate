<?php

namespace App\Modules\Acceptme\Http\Controllers;

use App\Generated\Modules\Acceptme\Http\Controllers\AbstractDocumentController;
use App\Modules\Acceptme\Http\Requests\DocumentRequest;
use App\Modules\Acceptme\Models\Document;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentController
 *
 * This class extends the generated class. Update this to resolve the requirements of this application module.
 */
final class DocumentController extends AbstractDocumentController
{
    /**
     * Create query to find a item or list
     *
     * @param DocumentRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getQuery(DocumentRequest $request)
    {

        $this->initCondition($request);

        $this->conditions['document_types.company_id'] =  (int) $request->get('jwt_company_id');

        $q = Document::select(
            'documents.*',
            DB::raw('COUNT (distinct(versions.id)) as versions_count'),
            DB::raw('SUM (versions.views) as views_count'),
            DB::raw('SUM(CASE WHEN registers.register_type_id = 1 THEN 1 ELSE 0 END) as accepts_count'),
            DB::raw('SUM(CASE WHEN registers.register_type_id = 2 THEN 1 ELSE 0 END) as rejects_count')
        )
            ->join('products', 'products.id', '=', 'documents.product_id')
            ->join('document_types', 'document_types.id', '=', 'documents.document_type_id')

            ->leftJoin('files', 'files.id', '=', 'products.file_id')
            ->leftJoin('versions', 'versions.document_id', '=', 'documents.id')
            ->leftJoin('registers', 'registers.version_id', '=', 'versions.id')

            ->groupBy('documents.id')
            ->where($this->conditions);

        return $q;
    }
}
