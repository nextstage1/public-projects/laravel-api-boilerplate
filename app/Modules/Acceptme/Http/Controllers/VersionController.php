<?php

namespace App\Modules\Acceptme\Http\Controllers;

use App\Generated\Modules\Acceptme\Http\Controllers\AbstractVersionController;
use App\Modules\Acceptme\Http\Requests\VersionRequest;
use App\Modules\Acceptme\Models\Version;
use Illuminate\Support\Facades\DB;

/**
 * Class VersionController
 *
 * This class extends the generated class. Update this to resolve the requirements of this application module.
 */
final class VersionController extends AbstractVersionController
{
    /**
     * Create query to find a item or list
     *
     * @param VersionRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getQuery(VersionRequest $request)
    {

        $this->initCondition($request);
        $this->conditions['document_types.company_id'] =  (int) $request->get('jwt_company_id');

        return Version::select(
            'versions.*',
            DB::raw('SUM(CASE WHEN registers.register_type_id = 1 THEN 1 ELSE 0 END) as accepts_count'),
            DB::raw('SUM(CASE WHEN registers.register_type_id = 2 THEN 1 ELSE 0 END) as rejects_count')
        )
            ->join('documents', 'documents.id', '=', 'versions.document_id')
            ->join('document_types', 'document_types.id', '=', 'documents.document_type_id')
            ->join('products', 'products.id', '=', 'documents.product_id')
            ->join('files', 'files.id', '=', 'versions.file_id')

            ->leftJoin('registers', 'registers.version_id', '=', 'versions.id')
            ->groupBy('versions.id')
            ->where($this->conditions);
    }
}
