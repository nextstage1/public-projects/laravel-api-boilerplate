<?php

namespace App\Modules\Acceptme\Http\Resources;

use App\Generated\Modules\Acceptme\Http\Resources\AbstractVersionResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


/**
 * Class VersionResource
 *
 * This class extends the generated class. Update this to resolve the requirements of this application module.
 */
final class VersionResource extends AbstractVersionResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = [];
        if ($this->resource instanceof \Illuminate\Database\Eloquent\Model) {
            $data = array_merge(
                parent::toArray($request),
                [
                    'counters' => [
                        'accepts' => $this->accepts_count,
                        'rejects' => $this->rejects_count,
                        'views' => $this->views
                    ]

                ]
            );
        }

        return $data;
    }
}
