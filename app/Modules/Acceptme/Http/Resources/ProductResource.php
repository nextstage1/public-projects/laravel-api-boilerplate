<?php

namespace App\Modules\Acceptme\Http\Resources;

use App\Generated\Modules\Acceptme\Http\Resources\AbstractProductResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


/**
 * Class ProductResource
 *
 * This class extends the generated class. Update this to resolve the requirements of this application module.
 */
final class ProductResource extends AbstractProductResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = [];
        if ($this->resource instanceof \Illuminate\Database\Eloquent\Model) {
            $data = array_merge(
                parent::toArray($request),
                [
                    'accepts' => $this->accepts_count,
                    'rejects' => $this->rejects_count,
                    'versions' => $this->versions_count,
                    'views' => $this->views_count,
                    'documents' => (int) $this->documents_count
                ]
            );
        }

        return $data;
    }
}
