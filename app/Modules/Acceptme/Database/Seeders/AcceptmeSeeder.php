<?php

    namespace App\Modules\Acceptme\Database\Seeders;

    use App\Generated\Modules\Acceptme\Database\Seeders\AbstractAcceptmeSeeder;
        
    final class AcceptmeSeeder extends AbstractAcceptmeSeeder
    {
        public function __construct()
        {
            $this->qtdeToSeed = 100;
        }
    
    }
       
    