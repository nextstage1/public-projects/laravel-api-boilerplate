<?php

    namespace App\Modules\Acceptme\Rules\DocumentType;

    use App\Generated\Modules\Acceptme\Rules\DocumentType\AbstractDocumentTypeVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class DocumentTypeVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class DocumentTypeVerifyBelongToRule extends AbstractDocumentTypeVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    