<?php

    namespace App\Modules\Acceptme\Rules\RegisterType;

    use App\Generated\Modules\Acceptme\Rules\RegisterType\AbstractRegisterTypeVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class RegisterTypeVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class RegisterTypeVerifyBelongToRule extends AbstractRegisterTypeVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    