<?php

    namespace App\Modules\Acceptme\Rules\Register;
    
    use Illuminate\Http\Request;
    
    class RegisterJsonPreparedRule
    {
        public function __invoke(Request $request): Request
        {            
            if (null !== $request->input('fingerprint') && is_array($request->input('fingerprint'))) {
                    $request->merge(['fingerprint' => json_encode($request->input('fingerprint'))]);
                }

            return $request;
        }
    }
    