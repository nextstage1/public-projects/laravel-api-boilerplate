<?php

    namespace App\Modules\Acceptme\Rules\Register;

    use App\Generated\Modules\Acceptme\Rules\Register\AbstractRegisterVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class RegisterVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class RegisterVerifyBelongToRule extends AbstractRegisterVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    