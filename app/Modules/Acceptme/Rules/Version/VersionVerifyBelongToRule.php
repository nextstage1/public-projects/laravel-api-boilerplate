<?php

    namespace App\Modules\Acceptme\Rules\Version;

    use App\Generated\Modules\Acceptme\Rules\Version\AbstractVersionVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class VersionVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class VersionVerifyBelongToRule extends AbstractVersionVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    