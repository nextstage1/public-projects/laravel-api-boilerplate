<?php

    namespace App\Modules\Acceptme\Rules\Product;

    use App\Generated\Modules\Acceptme\Rules\Product\AbstractProductVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class ProductVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class ProductVerifyBelongToRule extends AbstractProductVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    