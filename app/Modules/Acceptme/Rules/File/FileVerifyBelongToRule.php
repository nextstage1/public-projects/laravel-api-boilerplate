<?php

    namespace App\Modules\Acceptme\Rules\File;

    use App\Generated\Modules\Acceptme\Rules\File\AbstractFileVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class FileVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class FileVerifyBelongToRule extends AbstractFileVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    