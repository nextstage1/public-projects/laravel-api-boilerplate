<?php

    namespace App\Modules\Acceptme\Rules\Document;

    use App\Generated\Modules\Acceptme\Rules\Document\AbstractDocumentVerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class DocumentVerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class DocumentVerifyBelongToRule extends AbstractDocumentVerifyBelongToRule
    {
        public function __invoke(Request $request): Request
        {
            return parent::__invoke($request);
        }
    }
    