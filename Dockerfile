FROM nextstage/php:8.2-fpm-apache

ENV AUTORUN_ENABLED=false \
    APACHE_DOCUMENT_ROOT=/var/www/html/public \
    PHP_VERSION=8.2 \
    AUTORUN_LARAVEL_STORAGE_LINK=false \
    AUTORUN_LARAVEL_MIGRATION=false \
    LIMIT_PER_PAGE=10

#### BUILD APPLICATION
COPY . /var/www/html

RUN composer install --prefer-dist --optimize-autoloader --no-dev -q
# RUN chmod 0755 -R ./
RUN chmod 0777 /var/www/html/storage
RUN php artisan cache:clear
RUN php artisan route:clear