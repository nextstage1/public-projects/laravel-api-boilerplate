#!/bin/bash

# Define a variável __DIR__ com o diretório atual do script
__DIR__="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "$__DIR__/../"

source "$__DIR__/_init.sh"

echo "Atention!! You will lose all saved data!"
read -p "Confirm remove the database?: (yes/no) " DECIDE

if [ ! ${DECIDE:="NO"} = "yes" ]; then
    echo "Aborted!"
    exit 3
fi

rm -R ${PERSISTPATH}/${COMPOSE_PROJECT_NAME} >/dev/null 2>&1

. "$__DIR__/start.sh"
